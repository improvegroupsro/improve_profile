<?php
/**
 * @file
 * Enables modules and site configuration for a iMprove site installation.
 */

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function improve_form_install_configure_form_alter(&$form, $form_state) {
  $form['site_information']['site_name']['#default_value'] = $_SERVER['SERVER_NAME'];
  $form['site_information']['site_mail']['#default_value'] = 'podpora@improve.sk';

  $form['admin_account']['account']['name']['#default_value'] = 'admin';
  $form['admin_account']['account']['mail']['#default_value'] = 'podpora@improve.sk';

  $form['server_settings']['site_default_country']['#default_value'] = 'SK';
  $form['server_settings']['date_default_timezone']['#default_value'] = 'Europe/Bratislava';

  $form['update_notifications']['update_status_module']['#default_value'] = array(FALSE, FALSE);  
}
