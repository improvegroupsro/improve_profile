#!/bin/bash

echo ""
echo "=============================================================="
echo "= Ready to install Drupal using iMprove installation profile ="
echo "=============================================================="
echo ""

read -e -p "Database server: " -i "localhost" DB_SERVER
read -e -p "Database name: " DB_NAME
read -e -p "Database user: " DB_USER
read -e -p "Database password: " DB_PASS

echo ""

read -e -p "Default language: " -i "sk" LOCALE
read -e -p "Site name: " -i "Drupal" SITE_NAME
read -e -p "Site e-mail: " -i "support@improve.sk" SITE_MAIL

#install Drupal site using minimal profile
drush site-install improve --locale=$LOCALE --site-name=$SITE_NAME --site-mail=$SITE_MAIL --account-mail=$SITE_MAIL --account-name=admin --account-pass=password --db-url=mysql://$DB_USER:$DB_PASS@$DB_SERVER/$DB_NAME

#enable features
features_dir=sites/all/modules/custom/*

for filepath in $features_dir
do
  filename=${filepath##*/}
  name=${filename%%.*}
  
  read -p "Do you want to install module $name? (y/n): " yn
    case $yn in
        [Yy]* ) drush en $name -y; drush cc all; drush fr $name -y; drush cc all; echo ""; continue;;
        [Nn]* ) continue;;
        * ) echo "Please answer yes or no.";;
    esac

done